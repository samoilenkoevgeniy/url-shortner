@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @if(Session::get("message"))
                    <div class="alert alert-success">
                        {{ Session::get("message") }}
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Created links
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Url</th>
                                <th>Hash</th>
                                <th>Link</th>
                                <th>Hits</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($links as $link)
                                <tr>
                                    <td><a href="{{ url("/office/links/".$link->id) }}">{{ $link->id }}</a></td>
                                    <td><a href="{{ $link->url }}">{{ $link->url }}</a></td>
                                    <td>{{ $link->hash }}</td>
                                    <td><a target="_blank" href="{{ ENV("APP_BASE").$link->hash }}">{{ ENV("APP_BASE").$link->hash }}</a></td>
                                    <td>{{ $link->linkHits->count() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Api token
                    </div>
                    <div class="panel-body">
                        @if($user->auth_token)
                            <div class="well">
                                {{ $user->auth_token }}
                            </div>
                            <a href="{{ url("/office/generateToken") }}" class="btn btn-primary">Regenerate token</a>
                        @else
                            <a href="{{ url("/office/generateToken") }}" class="btn btn-primary">Generate token</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection