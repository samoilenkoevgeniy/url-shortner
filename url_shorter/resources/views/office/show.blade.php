@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Link #{{ $link->id }} <a href="{{ url("/office") }}">back to links</a>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>date hit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($hits as $hit)
                                <tr>
                                    <td>{{ $hit->id }}</td>
                                    <td>{{ $hit->created_at }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection