@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @if(!isset($hash))
            <div class="panel panel-default">
                <div class="panel-heading">Make your short url</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                    <form action="{{ url("/create") }}" method="post">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="url"></label>
                            <input type="text" class="form-control" name="url" id="url" placeholder="type you url here" />
                        </div>
                        <div class="form-group">
                            <input type="submit" value="get short url" class="btn btn-primary" />
                        </div>
                    </form>
                </div>
            </div>
            @else
                <div class="panel panel-success">
                    <div class="panel-heading">
                        Your short url
                    </div>
                    <div class="panel-body">
                        <h3><a target="_blank" href="{{ ENV("APP_BASE") }}{{ $hash }}">{{ ENV("APP_BASE") }}{{ $hash }}</a></h3>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
