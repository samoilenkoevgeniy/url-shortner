<?php

namespace App\Http\Controllers;

use App\Link;
use App\LinkHit;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redis;

class ApiController extends Controller
{
    /**
     * @param Request $request
     * @return array
     */
    public function create(Request $request)
    {
        if($request->input("token")) {
            $user = User::where("auth_token", $request->input("token"))->first();
            if($user) {
                $user_id = $user->id;
            } else {
                $user_id = NULL;
            }
        } else {
            $user_id = NULL;
        }
        $link = Link::where('url', $request->input('url'))->first();
        if($link) {
            return [
                "status" => "already exist",
                "hash" => $link->hash,
                "short_id" => $link->id,
                "short_url" => ENV("APP_BASE").$link->hash
            ];
        } else {
            $hash = Link::createHash();
            $link = Link::create([
                "url" => $request->input("url"),
                "hash" => $hash,
                "user_id" => $user_id
            ]);
            Redis::set($hash, $request->input("url"));
            return [
                "status" => "success",
                "hash" => $link->hash,
                "short_id" => $link->id,
                "short_url" => ENV("APP_BASE").$link->hash
            ];
        }
    }

    /**
     * @param int $url_id
     * @return array
     */
    public function redirectToLong($url_id = 0)
    {
        $link = Link::findOrNew($url_id);
        return response()->redirectTo(ENV("APP_BASE").$link->hash);
    }

    /**
     * @param int $url_id
     * @return mixed
     */
    public function getStats($url_id = 0)
    {
        return LinkHit::where("link_id",$url_id)->get();
    }
}
