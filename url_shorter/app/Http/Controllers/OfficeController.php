<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OfficeController extends Controller
{

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();
        $links = $user->links;
        return response()->view("office.index", compact("links", "user"));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $link = Link::findOrNew($id);
        $hits = $link->linkHits;
        return response()->view("office.show", compact("link", "hits"));
    }

    public function generateToken()
    {
        $user = \Auth::user();
        $token = md5(time().$user->id.$user->created_at);
        $user->auth_token = $token;
        $user->save();
        return redirect()->back()->with("message", "Token generated successfully!");
    }

}
