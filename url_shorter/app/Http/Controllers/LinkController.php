<?php

namespace App\Http\Controllers;

use App\Http\Requests\Links\LinkCreateRequest;
use App\Jobs\toWriteDownStatistics;
use App\Link;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redis;

class LinkController extends Controller
{
    public function create(LinkCreateRequest $request)
    {
        if(\Auth::guest()) {
            $user_id = NULL;
        } else {
            $user_id = \Auth::user()->id;
        }
        $link = Link::where('url', $request->input('url'))->first();
        if($link) {
            return response()->RedirectTo('/')->withInput()->with('hash',$link->hash);
        } else {
            $hash = Link::createHash();
            Link::create([
                "url" => $request->input("url"),
                "hash" => $hash,
                "user_id" => $user_id
            ]);
            Redis::set($hash, $request->input("url"));
            return response()->view("welcome", compact("hash"));
        }
    }

    /**
     * @param string $hash
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function goToLink($hash = "", Request $request)
    {
        $user_agent = $request->header("user-agent");
        $url = Redis::get($hash);
        $this->dispatch(new toWriteDownStatistics($hash, $user_agent));
        return response()->redirectTo($url);
    }
}
