<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['api']], function () {
    Route::get("/api/new", "ApiController@create");
    Route::get("/api/{url_id}", "ApiController@redirectToLong");
    Route::get("/api/{url_id}/stats", "ApiController@getStats");
});

Route::group(['middleware' => ['web', 'auth']], function(){
    Route::get("/office", "OfficeController@index");
    Route::get("/office/links/{id}", "OfficeController@show");
    Route::get("/office/generateToken", "OfficeController@generateToken");
});

Route::group(['middleware' => 'web'], function () {

    Route::get('/', 'WelcomeController@index');
    Route::post('/create', 'LinkController@create');

    Route::auth();
    Route::get("/{hash}", "LinkController@goToLink");
});
