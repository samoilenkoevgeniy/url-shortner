<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkHit extends Model
{
    protected $fillable = [
        "link_id", "user_agent"
    ];
}
