<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Link;
use App\LinkHit;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class toWriteDownStatistics extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $hash, $user_agent;

    /**
     * Create a new job instance.
     *
     * @param string $hash
     * @param string $user_agent
     */
    public function __construct($hash = "", $user_agent = "")
    {
        $this->hash = $hash;
        $this->user_agent = $user_agent;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        log::debug($this->hash);
        $link = Link::where("hash", $this->hash)->first();
        LinkHit::create([
            "link_id" => $link->id,
            "user_agent" => $this->user_agent
        ]);
    }
}
