<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Link extends Model
{
    protected $fillable = [
        "url", "hash", "user_id"
    ];

    /**
     * @return string
     */
    public static function createHash()
    {
        do {
            $newHash = Str::random(6);
        } while(Link::where('hash', $newHash)->count() > 0);
        return $newHash;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function linkHits()
    {
        return $this->hasMany(LinkHit::class);
    }
}
